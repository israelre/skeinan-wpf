# Skeinan WPF

This package contains common WPF code to be used between all Skeinan WPF projects.

## Package Usage Instructions

The deployment for usage in other projects is managed in Azure and can be accessed in:
https://dev.azure.com/skeinan/Skeinan/_build
