﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace SkeinanWpf {
	public class BoolItemsSource {
		public ObservableCollection<Tuple<string, bool?>> Options { get; set; } = new ObservableCollection<Tuple<string, bool?>>();

		private void AddTuple(string first, bool? second) {
			Options.Add(new Tuple<string, bool?>(first, second));
		}

		public BoolItemsSource(string nullValue, string trueValue, string falseValue) {
			AddTuple(nullValue, null);
			AddTuple(trueValue, true);
			AddTuple(falseValue, false);
		}

		public void Bind(ComboBox comboBox) {
			new BindingManager(comboBox, ComboBox.ItemsSourceProperty, this, nameof(Options)).Bind();
			comboBox.DisplayMemberPath = nameof(Tuple<string, bool?>.Item1);
			comboBox.SelectedValuePath = nameof(Tuple<string, bool?>.Item2);
		}
	}
}
