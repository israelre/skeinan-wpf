﻿using System;

namespace SkeinanWpf.Tabs {
	public class TabRefreshEventArgs : EventArgs {
		public TabContent Content { get; set; }

		public TabRefreshEventArgs(TabContent content) {
			Content = content;
		}
	}
}
