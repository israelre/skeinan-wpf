﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SkeinanWpf.Tabs {
	public partial class TabbedControlItem : UserControl {
		public static readonly DependencyProperty ItemProperty =
				DependencyProperty.Register(nameof(Item), typeof(TabData), typeof(TabbedControlItem), new PropertyMetadata(ItemChanged));
		public TabData Item {
			get { return (TabData)GetValue(ItemProperty); }
			set { SetValue(ItemProperty, value); }
		}

		public static readonly DependencyProperty IsSelectedProperty =
				DependencyProperty.Register(nameof(IsSelected), typeof(bool), typeof(TabbedControlItem));
		public bool IsSelected {
			get { return (bool)GetValue(IsSelectedProperty); }
			set { SetValue(IsSelectedProperty, value); }
		}

		private static void ItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			TabbedControlItem tabItem = ((TabbedControlItem)d);
			tabItem.Item.PropertyChangeManager.AddListener(nameof(TabData.Content), tabItem.ItemContentChanged);
			tabItem.ItemContentChanged();
		}

		private void ItemContentChanged() {
			if (Item.Content != null) {
				Item.Content.Refresh += Content_Refresh;
				Item.Content.Close += Content_Close;
			}
		}

		public event EventHandler Close;
		public event EventHandler TabClick;
		public delegate void RefreshHandler(object sender, TabRefreshEventArgs e);
		public event RefreshHandler Refresh;

		public TabbedControlItem() {
			InitializeComponent();
			DataContext = this;

			new BindingManager(TabLabel, Label.ContentProperty, this, nameof(Item) + "." + nameof(Item.Title)).Bind();

			TabButton.MouseDown += TabButton_MouseDown;
			TabButton.Click += TabButton_Click;
			CloseButton.Click += CloseButton_Click;
		}

		private void TabButton_MouseDown(object sender, MouseButtonEventArgs e) {
			if (e.ChangedButton == MouseButton.Middle && e.MiddleButton == MouseButtonState.Pressed && e.LeftButton == MouseButtonState.Released && e.RightButton == MouseButtonState.Released) {
				OnClose();
			}
		}

		private void TabButton_Click(object sender, RoutedEventArgs e) {
			TabClick(this, new EventArgs());
		}

		private void CloseButton_Click(object sender, RoutedEventArgs e) {
			OnClose();
			e.Handled = true;
		}

		protected void OnClose() {
			Close(this, new EventArgs());
		}

		private void OnRefresh(TabContent content) {
			Refresh(this, new TabRefreshEventArgs(content));
		}

		private void Content_Refresh(object sender, TabRefreshEventArgs e) {
			OnRefresh(e.Content);
		}

		private void Content_Close(object sender, EventArgs e) {
			OnClose();
		}
	}
}
