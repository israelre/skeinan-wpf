﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace SkeinanWpf.Tabs {
	public partial class TabContent : UserControl, INotifyPropertyChanged {

		private bool mIsValidationEnabled = true;
		protected bool IsValidationEnabled {
			get {
				return mIsValidationEnabled;
			}
			set {
				BindingManagers.ForEach(bm => bm.IsValidationEnabled = value);
				mIsValidationEnabled = value;
			}
		}

		private IInputElement mLastFocus;

		public event PropertyChangedEventHandler PropertyChanged;
		public PropertyChangeManager PropertyChangeManager { get; }

		private List<BindingManager> BindingManagers { get; } = new List<BindingManager>();

		public void NotifyPropertyChanged([CallerMemberName] string propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void AddListener(string propertyName, Action action) {
			PropertyChangeManager.AddListener(propertyName, action);
		}
		
		public event EventHandler Close;
		public delegate void RefreshHandler(object sender, TabRefreshEventArgs e);
		public event RefreshHandler Refresh;

		protected BindingManager AddBindingManager(BindingManager bindingManager) {
			BindingManagers.Add(bindingManager);
			bindingManager.Bind();
			bindingManager.IsValidationEnabled = IsValidationEnabled;
			return bindingManager;
		}

		protected BindingManager AddBindingManager(FrameworkElement element, DependencyProperty dependencyProperty, object source, string propertyPath) {
			return AddBindingManager(new BindingManager(element, dependencyProperty, source, propertyPath));
		}

		protected BindingManager GetBindingManager(FrameworkElement element) {
			return BindingManagers.Where(bm => bm.Element == Keyboard.FocusedElement).FirstOrDefault();
		}

		protected void OnRefresh(TabContent newContent) {
			Refresh(this, new TabRefreshEventArgs(newContent));
		}

		protected void PostInitializeComponent(bool imageBackground = true) {
			DataContext = this;
			((FrameworkElement)Content).Loaded += TabContent_Loaded;
			((FrameworkElement)Content).Unloaded += TabContent_Unloaded;
		}

		private void TabContent_Unloaded(object sender, RoutedEventArgs e) {
			mLastFocus = FocusManager.GetFocusedElement(this);
		}

		private void TabContent_Loaded(object sender, RoutedEventArgs e) {
			if (mLastFocus != null) {
				Keyboard.Focus(mLastFocus);
			} else {
				IInputElement firstField = WpfUtil.FindChildren((DependencyObject)Content).OfType<IInputElement>().Where(element => element.Focusable).FirstOrDefault();
				Keyboard.Focus(firstField);
			}
		}

		public TabContent() {
			PropertyChangeManager = new PropertyChangeManager(this);
			FocusManager.SetIsFocusScope(this, true);
		}

		public void OnClose() {
			Close(this, new EventArgs());
		}

		protected static FrameworkElement FindFirst(FrameworkElement parent, List<FrameworkElement> elements) {
			if (elements.Contains(parent)) {
				return parent;
			} else {
				foreach (FrameworkElement child in LogicalTreeHelper.GetChildren(parent).OfType<FrameworkElement>()) {
					FrameworkElement childResult = FindFirst(child, elements);
					if (childResult != null) {
						return childResult;
					}
				}
			}
			return null;
		}

		protected bool Validate(string invalidMessageBoxText, string caption) {
			List<FrameworkElement> invalidElements = new List<FrameworkElement>();
			foreach (BindingManager manager in BindingManagers) {
				if (!manager.Validate()) {
					invalidElements.Add(manager.Element);
				}
			}
			FrameworkElement invalidElement = FindFirst((FrameworkElement)Content, invalidElements);
			if (invalidElement != null) {
				MessageBox.Show(invalidMessageBoxText, caption, MessageBoxButton.OK, MessageBoxImage.Warning);
				Keyboard.Focus(invalidElement);
				ScrollViewer scrollViewer = WpfUtil.GetNearestParent<ScrollViewer>(invalidElement);
				if (scrollViewer != null) {
					scrollViewer.LineUp();
					scrollViewer.LineUp();
				}
				return false;
			} else {
				return true;
			}
		}

		protected bool Save(string successMessageBoxText, string caption, Action saveAction, string confirmationMessage = null) {
			bool saveAccepted = true;
			if (confirmationMessage != null) {
				MessageBoxResult result = MessageBox.Show(confirmationMessage, caption, MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
				saveAccepted = result == MessageBoxResult.Yes;
			}
			
			if (saveAccepted) {
				saveAction();
				MessageBox.Show(successMessageBoxText, caption, MessageBoxButton.OK, MessageBoxImage.Information);
				return true;
			} else {
				return false;
			}
		}

		protected DataGridColumn CreateDataGridColumn(string header, string binding) {
			return new DataGridTextColumn {
				Header = header,
				Binding = new Binding(binding)
			};
		}

		protected DataGridTemplateColumn CreateDataGridActionColumn(string header, string text, string commandName, Binding commandParameterBinding, Binding visibilityBinding = null) {
			Binding commandBinding = new Binding(commandName) {
				RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, GetType(), 1)
			};

			FrameworkElementFactory factory = new FrameworkElementFactory(typeof(Button));
			factory.SetValue(Button.ContentProperty, text);
			factory.SetValue(Button.CommandProperty, commandBinding);
			factory.SetValue(Button.CommandParameterProperty, commandParameterBinding);
			if (visibilityBinding != null) {
				factory.SetValue(Button.VisibilityProperty, visibilityBinding);
			}

			DataTemplate dataTemplate = new DataTemplate {
				VisualTree = factory
			};

			DataGridTemplateColumn templateColumn = new DataGridTemplateColumn {
				Width = DataGridLength.SizeToCells,
				CellTemplate = dataTemplate
			};

			return templateColumn;
		}

		protected void SetupDataGrid(DataGrid dataGrid) {
			RenderOptions.SetEdgeMode(dataGrid, EdgeMode.Aliased);
			WpfUtil.GetNearestParent<ScrollViewer>(dataGrid).PreviewMouseWheel += ScrollViewer_PreviewMouseWheel;
		}

		private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e) {
			ScrollViewer scrollViewer = (ScrollViewer)sender;
			scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - e.Delta);
			e.Handled = true;
		}
	}
}
