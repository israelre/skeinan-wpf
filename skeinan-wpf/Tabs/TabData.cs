﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SkeinanWpf.Tabs {
	public class TabData : INotifyPropertyChanged {
		public void NotifyPropertyChanged([CallerMemberName] string propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public event PropertyChangedEventHandler PropertyChanged;
		public PropertyChangeManager PropertyChangeManager { get; }

		public TabData() {
			PropertyChangeManager = new PropertyChangeManager(this);
		}

		private string mTitle;
		public string Title {
			get { return mTitle; }
			set { mTitle = value; NotifyPropertyChanged(); }
		}

		private TabContent mContent;
		public TabContent Content {
			get { return mContent; }
			set { mContent = value; NotifyPropertyChanged(); }
		}
	}
}
