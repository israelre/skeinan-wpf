﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace SkeinanWpf.Tabs {
	public partial class TabbedControl : UserControl {
		private static readonly DependencyProperty TabsWidthProperty =
				DependencyProperty.Register(nameof(TabsWidth), typeof(double), typeof(TabbedControl), new PropertyMetadata(TabsWidthChanged));
		private double TabsWidth {
			get { return (double)GetValue(TabsWidthProperty); }
			set { SetValue(TabsWidthProperty, value); }
		}

		public static readonly DependencyProperty TabsHeightProperty =
				DependencyProperty.Register(nameof(TabsHeight), typeof(double), typeof(TabbedControl), new PropertyMetadata(TabsWidthChanged));
		public double TabsHeight {
			get { return (double)GetValue(TabsHeightProperty); }
			set { SetValue(TabsHeightProperty, value); }
		}

		private static readonly DependencyProperty TabsScrollViewerWidthProperty =
				DependencyProperty.Register(nameof(TabsScrollViewerWidth), typeof(double), typeof(TabbedControl), new PropertyMetadata(TabsScrollViewerWidthChanged));
		private double TabsScrollViewerWidth {
			get { return (double)GetValue(TabsScrollViewerWidthProperty); }
			set { SetValue(TabsScrollViewerWidthProperty, value); }
		}

		private static readonly DependencyProperty TabsScrollViewerOffsetProperty =
				DependencyProperty.Register(nameof(TabsScrollViewerOffset), typeof(double), typeof(TabbedControl), new PropertyMetadata(TabsScrollViewerOffsetChanged));
		private double TabsScrollViewerOffset {
			get { return (double)GetValue(TabsScrollViewerOffsetProperty); }
			set { SetValue(TabsScrollViewerOffsetProperty, value); }
		}

		private static readonly DependencyProperty LeftScrollVisibleProperty =
				DependencyProperty.Register(nameof(LeftScrollVisible), typeof(bool), typeof(TabbedControl));
		private bool LeftScrollVisible {
			get { return (bool)GetValue(LeftScrollVisibleProperty); }
			set { SetValue(LeftScrollVisibleProperty, value); }
		}

		private static readonly DependencyProperty RightScrollVisibleProperty =
				DependencyProperty.Register(nameof(RightScrollVisible), typeof(bool), typeof(TabbedControl));
		private bool RightScrollVisible {
			get { return (bool)GetValue(RightScrollVisibleProperty); }
			set { SetValue(RightScrollVisibleProperty, value); }
		}

		private static void TabsWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			((TabbedControl)d).UpdateScrollVisibility();
		}

		private static void TabsScrollViewerWidthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			((TabbedControl)d).UpdateScrollVisibility();
		}

		private static void TabsScrollViewerOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			((TabbedControl)d).UpdateScrollVisibility();
		}

		private void UpdateScrollVisibility() {
			Dispatcher.Invoke(() => {
				bool isFull = TabsWidth >= TabsScrollViewerWidth;
				bool isBeginning = TabsScrollViewerOffset == 0;
				bool isEnd = TabsScrollViewerOffset + TabsScrollViewerWidth >= TabsWidth;
				LeftScrollVisible = isFull && !isBeginning;
				RightScrollVisible = isFull && !isEnd;
			}, DispatcherPriority.Render);
		}

		public void Add(TabData item, bool overwrite = false) {
			TabbedControlItem existingItem = TabsPanel.Children.OfType<TabbedControlItem>().Where(t => t.Item.Title == item.Title).FirstOrDefault();

			if (existingItem == null) {
				TabbedControlItem itemControl = new TabbedControlItem();

				itemControl.Item = item;
				itemControl.TabClick += (sender, e) => {
					Select((TabbedControlItem)sender);
				};
				itemControl.Refresh += (sender, e) => {
					((TabbedControlItem)sender).Item.Content = e.Content;
					Select((TabbedControlItem)sender);
				};
				itemControl.Close += (sender, e) => {
					CloseTab((TabbedControlItem)sender);
				};
				TabsPanel.Children.Add(itemControl);
				Select(itemControl);
			} else {
				if (overwrite) {
					existingItem.Item.Content = item.Content;
				}
				Select(existingItem);
			}
		}

		public void CloseTab(TabbedControlItem item) {
			if (TabsPanel.Children.Count > 1) {
				if (ContentPanel.Child == item.Item.Content) {
					int currentIndex = TabsPanel.Children.IndexOf(item);
					if (currentIndex == TabsPanel.Children.Count - 1) {
						Select((TabbedControlItem)TabsPanel.Children[currentIndex - 1]);
					} else {
						Select((TabbedControlItem)TabsPanel.Children[currentIndex + 1]);
					}
				}
			} else {
				ContentPanel.Child = null;
			}
			TabsPanel.Children.Remove(item);
		}

		public void Select(TabbedControlItem item) {
			ContentPanel.Child = item.Item.Content;
			item.Item.PropertyChangeManager.AddListener(nameof(TabItem.Content),
					(value) => {
						ContentPanel.Child = (TabContent)value;
					});

			TabsPanel.Children.OfType<TabbedControlItem>().Where(tab => tab.IsSelected == true).ToList().ForEach(tab => tab.IsSelected = false);
			item.IsSelected = true;

			item.BringIntoView();
		}

		private void OnKeyEvent(object sender, KeyEventArgs e) {
			if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) {
				if (Keyboard.IsKeyDown(Key.F4)) {
					CloseTab(TabsPanel.Children.OfType<TabbedControlItem>().Where(tab => tab.IsSelected).FirstOrDefault());
				} else if (Keyboard.IsKeyDown(Key.Tab)) {
					List<TabbedControlItem> tabs = TabsPanel.Children.OfType<TabbedControlItem>().ToList();
					int previous = tabs.IndexOf(tabs.Where(tab => tab.IsSelected).FirstOrDefault());
					if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) {
						if (previous - 1 < 0) {
							Select(tabs[tabs.Count - 1]);
						} else {
							Select(tabs[previous - 1]);
						}
					} else {
						if (previous + 1 >= tabs.Count) {
							Select(tabs[0]);
						} else {
							Select(tabs[previous + 1]);
						}
					}
				}
			}
		}

		public TabbedControl() {
			InitializeComponent();
			Loaded += TabbedControl_Loaded;
			Keyboard.AddPreviewKeyDownHandler(this, OnKeyEvent);

			new BindingManager(this, TabsWidthProperty, TabsPanel, nameof(TabsPanel.ActualWidth)) {
				Mode = BindingMode.OneWay
			}.Bind();
			new BindingManager(this, TabsScrollViewerWidthProperty, TabsScrollViewer, nameof(TabsScrollViewer.ActualWidth)) {
				Mode = BindingMode.OneWay
			}.Bind();
			new BindingManager(this, TabsScrollViewerOffsetProperty, TabsScrollViewer, nameof(TabsScrollViewer.HorizontalOffset)) {
				Mode = BindingMode.OneWay
			}.Bind();
			new BindingManager(LeftScrollButton, Button.VisibilityProperty, this, nameof(LeftScrollVisible)) {
				Converter = new BooleanToVisibilityConverter()
			}.Bind();
			new BindingManager(RightScrollButton, Button.VisibilityProperty, this, nameof(RightScrollVisible)) {
				Converter = new BooleanToVisibilityConverter()
			}.Bind();

			LeftScrollButton.Click += (sender, e) => {
				double previousTotalWidth = 0;
				double totalWidth = 0;
				foreach (FrameworkElement element in TabsPanel.Children) {
					totalWidth += element.ActualWidth;
					if (totalWidth >= TabsScrollViewer.HorizontalOffset) {
						AnimatedScrollToHorizontalOffset(previousTotalWidth, 1000);
						break;
					} else {
						previousTotalWidth = totalWidth;
					}
				}
			};

			RightScrollButton.Click += (sender, e) => {
				double totalWidth = 0;
				foreach (FrameworkElement element in TabsPanel.Children) {
					totalWidth += element.ActualWidth;
					if (totalWidth - TabsScrollViewer.HorizontalOffset > TabsScrollViewerWidth) {
						double scroll = totalWidth - TabsScrollViewerWidth;
						AnimatedScrollToHorizontalOffset(scroll, 1000);
						Dispatcher.Invoke(() => {
							scroll = totalWidth - TabsScrollViewerWidth;
						}, DispatcherPriority.Render);
						AnimatedScrollToHorizontalOffset(scroll, 1000);
						break;
					}
				}
			};
		}

		private void TabbedControl_Loaded(object sender, RoutedEventArgs e) {
			DataContext = this;
		}

		private void AnimatedScrollToHorizontalOffset(double offset, double pxPerSecond) {
			int millisecondsPerFrame = 15;
			double offsetChange = offset - TabsScrollViewer.HorizontalOffset;
			double millisecondsDuration = offsetChange*1000/pxPerSecond;
			double frameCountDouble = millisecondsDuration/millisecondsPerFrame;
			if (frameCountDouble < 0) {
				frameCountDouble = -frameCountDouble;
			}
			int frameCount = (int)frameCountDouble;
			if (frameCountDouble > frameCount) {
				++frameCount;
			}
			double offsetPerFrame = offsetChange/frameCount;
			for (int i = 0; i < frameCount; ++i) {
				Dispatcher.Invoke(() => {
					if (i == frameCount - 1) {
						TabsScrollViewer.ScrollToHorizontalOffset(offset);
					} else {
						TabsScrollViewer.ScrollToHorizontalOffset(TabsScrollViewer.HorizontalOffset + offsetPerFrame);
						Thread.Sleep(millisecondsPerFrame);
					}
				}, DispatcherPriority.Input);
			}
		}
	}
}
