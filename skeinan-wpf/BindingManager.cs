﻿using SkeinanWpf.Converters;
using SkeinanWpf.Validators;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace SkeinanWpf {
	public class BindingManager {
		protected bool IsBound { get; private set; }

		public BindingMode Mode { get; set; } = BindingMode.TwoWay;
		public UpdateSourceTrigger UpdateSourceTrigger { get; set; } = UpdateSourceTrigger.LostFocus;
		public IValueConverter Converter { get; set; } = null;
		public List<ValidationRule> ValidationRules { get; } = new List<ValidationRule>();

		public FrameworkElement Element { get; }
		public DependencyProperty Property { get; }
		private readonly object mSource;
		private readonly PropertyPath mPropertyPath;

		public string ValidationResultText { get; set; }

		private Label mValidationResultElement;
		public Label ValidationResultElement {
			get { return mValidationResultElement; }
			set {
				mValidationResultElement = value;
				if (IsBound) Bind();
			}
		}

		private bool mIsValidationEnabled;
		public bool IsValidationEnabled {
			get { return mIsValidationEnabled; }
			set {
				if (value) {
					BindValidation();
				} else {
					ClearValidationBinding();
				}
			}
		}

		public void UpdateBinding() {
			Element.GetBindingExpression(Property).UpdateSource();
		}

		public BindingManager(FrameworkElement element, DependencyProperty dependencyProperty, object source, string propertyPath) {
			Element = element;
			Property = dependencyProperty;
			mSource = source;
			mPropertyPath = new PropertyPath(propertyPath);
		}

		public BindingManager Bind() {
			IsBound = true;
			BindingOperations.ClearBinding(Element, Property);
			Binding binding = new Binding {
				Mode = Mode,
				UpdateSourceTrigger = UpdateSourceTrigger,
				Path = mPropertyPath,
				Source = mSource,
				Converter = Converter
			};
			BindingOperations.SetBinding(Element, Property, binding);
			Element.IsKeyboardFocusWithinChanged += OnElementLostFocus;

			BindValidation();

			return this;
		}

		private void BindValidation() {
			if (ValidationResultElement != null) {
				List<ValidationRule> tempValidationRules = new List<ValidationRule>();
				tempValidationRules.AddRange(ValidationRules);
				ValidationRules.Clear();
				ValidationConverter converter = new ValidationConverter() {
					ValidationRules = ValidationRules
				};
				Binding validationBinding = new Binding {
					Mode = BindingMode.OneWay,
					UpdateSourceTrigger = UpdateSourceTrigger,
					Path = mPropertyPath,
					Source = mSource,
					Converter = converter
				};
				BindingOperations.ClearBinding(ValidationResultElement, Label.ContentProperty);
				BindingOperations.SetBinding(ValidationResultElement, Label.ContentProperty, validationBinding);
				ValidationRules.AddRange(tempValidationRules);
				mIsValidationEnabled = true;
			}
		}

		public void ClearBinding() {
			Element.IsKeyboardFocusWithinChanged -= OnElementLostFocus;
			BindingOperations.ClearBinding(Element, Property);
			ClearValidationBinding();
		}

		private void ClearValidationBinding() {
			if (ValidationResultElement != null) {
				BindingOperations.ClearBinding(ValidationResultElement, Label.ContentProperty);
			}
			mIsValidationEnabled = false;
		}

		private void OnElementLostFocus(object sender, DependencyPropertyChangedEventArgs e) {
			if (!(bool)e.NewValue && !WpfUtil.IsVisualChild(Element, Keyboard.FocusedElement as FrameworkElement)) {
				Element.GetBindingExpression(Property).UpdateSource();
			}
		}

		public bool Validate() {
			bool isValid = true;
			if (IsValidationEnabled) {
				List<ValidationRule> tempValidationRules = new List<ValidationRule>();
				tempValidationRules.AddRange(ValidationRules);
				ValidationRules.Clear();
				tempValidationRules.ForEach(vr => ValidationRules.Add(new ValidationRuleWrapper(vr)));
				ValidationResultElement?.GetBindingExpression(Label.ContentProperty).UpdateTarget();
				isValid = !ValidationRules.OfType<ValidationRuleWrapper>().Any(vrw => vrw.LastResult == null || !vrw.LastResult.IsValid);
				ValidationRules.Clear();
				ValidationRules.AddRange(tempValidationRules);
			}
			return isValid;
		}
	}
}
