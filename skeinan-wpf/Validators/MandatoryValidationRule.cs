﻿using System.Globalization;
using System.Windows.Controls;

namespace SkeinanWpf.Validators {
	public class MandatoryValidationRule : ValidationRule {
		public bool Enabled { get; set; } = true;

		public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
			bool valid = !Enabled || !string.IsNullOrWhiteSpace(value?.ToString());
			return valid ? ValidationResult.ValidResult : new ValidationResult(false, Properties.Resources.MandatoryTextValidatorMessage);
		}
	}
}
