﻿using System.Globalization;
using System.Windows.Controls;

namespace SkeinanWpf.Validators {
	class ValidationRuleWrapper : ValidationRule {
		public ValidationRule InnerRule { get; }
		public ValidationResult LastResult { get; private set; }

		public ValidationRuleWrapper(ValidationRule innerRule) {
			InnerRule = innerRule;
		}

		public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
			ValidationResult result = InnerRule.Validate(value, cultureInfo);
			LastResult = result;
			return result;
		}
	}
}
