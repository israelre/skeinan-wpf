﻿using System.Collections;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkeinanWpf.Validators {
	public class NullListValueValidationRule : ValidationRule {
		public bool Enabled { get; set; } = true;

		public override ValidationResult Validate(object value, CultureInfo cultureInfo) {
			IEnumerable list = (IEnumerable)value;
			bool valid = true;
			if (list != null) {
				foreach (object item in list) {
					if (item == null) {
						valid = false;
						break;
					}
				}
			}
			return valid ? ValidationResult.ValidResult : new ValidationResult(false, Properties.Resources.MandatoryTextValidatorMessage);
		}
	}
}
