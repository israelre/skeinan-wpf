﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SkeinanWpf.Converters {
	public class BoolToInverseConverter : IValueConverter {
		public IValueConverter InnerConverter { get; set; }

		public BoolToInverseConverter(IValueConverter innerConverter) {
			InnerConverter = innerConverter;
		}

		public BoolToInverseConverter() {
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			object convertedValue = InnerConverter != null ? InnerConverter.Convert(value, targetType, parameter, culture) : value;
			return !(convertedValue as bool?);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			object convertedValue = InnerConverter != null ? InnerConverter.ConvertBack(value, targetType, parameter, culture) : value;
			return !(convertedValue as bool?);
		}
	}
}
