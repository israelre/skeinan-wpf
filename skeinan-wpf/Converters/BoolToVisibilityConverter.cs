﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SkeinanWpf.Converters {
	public class BoolToVisibilityConverter : IValueConverter {
		public bool Inverse { get; set; }

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			bool finalValue = (bool)value ^ Inverse;
			return finalValue ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
