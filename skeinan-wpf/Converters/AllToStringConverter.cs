﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SkeinanWpf.Converters {
	public class AllToStringConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			if (value == null) {
				return "";
			} else {
				return value.ToString();
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			string stringValue = (string)value;
			if (targetType == typeof(string)) {
				return stringValue;
			}
			if (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == typeof(Nullable<>) && string.IsNullOrWhiteSpace(stringValue)) {
				return null;
			}
			if (targetType == typeof(int) || targetType == typeof(int?)) {
				return int.Parse(stringValue);
			} else if (targetType == typeof(long) || targetType == typeof(long?)) {
				return long.Parse(stringValue);
			} else if (targetType == typeof(float) || targetType == typeof(float?)) {
				return float.Parse(stringValue);
			} else if (targetType == typeof(double) || targetType == typeof(double?)) {
				return double.Parse(stringValue);
			} else if (targetType == typeof(decimal) || targetType == typeof(decimal?)) {
				return decimal.Parse(stringValue);
			} else if (targetType == typeof(DateTime) || targetType == typeof(DateTime?)) {
				return DateTime.Parse(stringValue);
			} else {
				throw new NotImplementedException();
			}
		}
	}
}
