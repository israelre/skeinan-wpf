﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;

namespace SkeinanWpf.Converters {
	public class ValidationErrorConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			ICollection<ValidationError> validationErrors = (ICollection<ValidationError>)value;
			if (validationErrors != null) {
				return string.Join(Environment.NewLine, validationErrors.Select(v => v.ErrorContent));
			} else {
				return null;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
