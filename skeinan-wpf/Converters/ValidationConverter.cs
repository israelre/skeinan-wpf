﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace SkeinanWpf.Converters {
	public class ValidationConverter : IValueConverter {
		public List<ValidationRule> ValidationRules { get; set; }

		public object Convert(object value, Type targetType, object parameter, CultureInfo cultureInfo) {
			string message = null;
			if (ValidationRules != null) {
				foreach (ValidationRule rule in ValidationRules) {
					ValidationResult result = rule.Validate(value, cultureInfo);
					if (!result.IsValid) {
						message = result.ErrorContent.ToString();
						break;
					}
				}
			}
			return message;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo cultureInfo) {
			throw new NotImplementedException();
		}
	}
}
