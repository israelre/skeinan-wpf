﻿using System.Windows;

namespace SkeinanWpf {
	public class BoundProperty {
		public FrameworkElement Element { get; }
		public DependencyProperty Property { get; }

		public BoundProperty(FrameworkElement element, DependencyProperty property) {
			Element = element;
			Property = property;
		}

		public override bool Equals(object obj) {
			BoundProperty comparable = obj as BoundProperty;
			if (comparable == null) {
				return false;
			} else {
				return Equals(Element, comparable.Element) && Equals(Property, comparable.Property);
			}
		}

		public override int GetHashCode() {
			unchecked {
				int hash = 17;
				if (Element != null) hash = hash * 23 + Element.GetHashCode();
				if (Property != null) hash = hash * 23 + Property.GetHashCode();
				return hash;
			}
		}
	}
}
