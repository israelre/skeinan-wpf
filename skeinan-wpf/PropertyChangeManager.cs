﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace SkeinanWpf {
	public class PropertyChangeManager {
		private Dictionary<string, List<Action>> mActions = new Dictionary<string, List<Action>>();
		private Dictionary<string, List<Action<object>>> mParamActions = new Dictionary<string, List<Action<object>>>();

		public PropertyChangeManager(INotifyPropertyChanged owner) {
			owner.PropertyChanged += OnPropertyChanged;
		}

		private void OnPropertyChanged(object sender, PropertyChangedEventArgs e) {
			mActions.TryGetValue(e.PropertyName, out List<Action> actions);
			mParamActions.TryGetValue(e.PropertyName, out List<Action<object>> paramActions);
			actions?.ForEach(a => a.Invoke());
			paramActions?.ForEach(a => a.Invoke(sender.GetType().GetProperty(e.PropertyName).GetValue(sender)));
		}

		public void AddListener(string propertyName, Action action) {
			mActions.TryGetValue(propertyName, out List<Action> actions);
			if (actions == null) {
				actions = new List<Action>();
				mActions.Add(propertyName, actions);
			}
			actions.Add(action);
		}

		public void AddListener(string propertyName, Action<object> action) {
			mParamActions.TryGetValue(propertyName, out List<Action<object>> actions);
			if (actions == null) {
				actions = new List<Action<object>>();
				mParamActions.Add(propertyName, actions);
			}
			actions.Add(action);
		}
	}
}
