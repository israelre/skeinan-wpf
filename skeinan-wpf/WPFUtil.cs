﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SkeinanWpf {
	public static class WpfUtil {
		public static IEnumerable<DependencyObject> FindChildren(DependencyObject parent) {
			int childrenCount = VisualTreeHelper.GetChildrenCount(parent);

			for (int i = 0; i < childrenCount; ++i) {
				DependencyObject child = VisualTreeHelper.GetChild(parent, i);
				foreach (DependencyObject innerChild in FindChildren(child)) {
					yield return innerChild;
				}
				yield return child;
			}
		}

		public static T GetNearestParent<T>(FrameworkElement child) where T : FrameworkElement {
			DependencyObject currentParent = child?.Parent;
			if (currentParent == null) {
				return default(T);
			} else if (typeof(T).IsAssignableFrom(currentParent.GetType())) {
				return (T)currentParent;
			} else if (typeof(FrameworkElement).IsAssignableFrom(currentParent.GetType())) {
				return GetNearestParent<T>((FrameworkElement)currentParent);
			} else {
				return null;
			}
		}

		public static void BlockInvalidSelection(ComboBox comboBox) {
			comboBox.IsKeyboardFocusWithinChanged += (sender, e) => {
				if (!(bool)e.NewValue) {
					string displayedValue = (string)comboBox.SelectedValue?.GetType().GetProperty(comboBox.DisplayMemberPath).GetValue(comboBox.SelectedValue);
					if (!string.IsNullOrEmpty(comboBox.Text) && !comboBox.Text.Equals(displayedValue)) {
						comboBox.SelectedValue = null;
					}
				}
			};
		}

		public static bool IsVisualChild(FrameworkElement parent, FrameworkElement child) {
			if (parent == null || child == null) return false;
			if (parent == child) return true;

			FrameworkElement visualParent = child.Parent as FrameworkElement;
			if (visualParent == null) {
				visualParent = VisualTreeHelper.GetParent(child) as FrameworkElement;
			}

			if (visualParent != null) {
				return IsVisualChild(parent, visualParent);
			} else {
				return false;
			}
		}
	}
}
