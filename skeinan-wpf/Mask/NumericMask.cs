﻿using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace SkeinanWpf.Masks {
	public class NumericMask : Mask {
		public NumericMask(TextBox textBox) : base(textBox) {
		}

		protected override bool Validate(string text) {
			return Regex.IsMatch(text, "^\\d*$");
		}
	}
}
