﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkeinanWpf.Masks {
	public abstract class Mask {
		private TextBox mTextBox;
		private string mPreviousText;
		private int mPreviousSelectionStart;
		private int mPreviousSelectionLength;

		public Mask(TextBox textBox) {
			mTextBox = textBox;
		}

		public void Attach() {
			mTextBox.PreviewTextInput += TextBox_PreviewTextInput;
			DataObject.AddPastingHandler(mTextBox, TextBox_Pasting);
			mTextBox.TextChanged += TextBox_TextChanged;
		}

		private void TextBox_Pasting(object sender, DataObjectPastingEventArgs e) {
			mPreviousText = mTextBox.Text;
			mPreviousSelectionStart = mTextBox.SelectionStart;
			mPreviousSelectionLength = mTextBox.SelectionLength;
		}

		private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e) {
			mPreviousText = mTextBox.Text;
			mPreviousSelectionStart = mTextBox.SelectionStart;
			mPreviousSelectionLength = mTextBox.SelectionLength;
		}

		private void TextBox_TextChanged(object sender, TextChangedEventArgs e) {
			if (!Validate(mTextBox.Text)) {
				mTextBox.Text = mPreviousText;
				mTextBox.SelectionStart = mPreviousSelectionStart;
				mTextBox.SelectionLength = mPreviousSelectionLength;
			}
		}

		protected abstract bool Validate(string text);
	}
}
