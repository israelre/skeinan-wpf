﻿using System;
using System.Collections;
using System.Threading.Tasks;

namespace SkeinanWpf.Fields {
	public class SuggestionFieldTask {
		private SuggestionFieldPendingLoad mPendingLoad;
		private Task<SuggestionFieldPendingLoad> mLoadTask;

		public Func<string, IList> LoadFunc { get; set; }
		public Action<IList, bool> FillResultFunc { get; set; }

		public SuggestionFieldTask(Func<string, IList> loadFunc, Action<IList, bool> fillResultFunc) {
			LoadFunc = loadFunc;
			FillResultFunc = fillResultFunc;
		}

		public void LoadPopupData(string input, bool visibilityAffected) {
			mPendingLoad = new SuggestionFieldPendingLoad(input, visibilityAffected);
			if (mLoadTask == null || mLoadTask.IsCompleted) {
				LoadPendingPopupData();
			}
		}

		private void LoadPendingPopupData() {
			SuggestionFieldPendingLoad pendingLoad = mPendingLoad;
			mPendingLoad = null;
			mLoadTask = LoadFuncAsync(pendingLoad);
			mLoadTask.ContinueWith(FillPopup);
		}

		private async Task<SuggestionFieldPendingLoad> LoadFuncAsync(SuggestionFieldPendingLoad input) {
			SuggestionFieldPendingLoad result = await Task.Run(() => {
				IList list = LoadFunc(input.Input);
				input.Result = list;
				return input;
			});
			return result;
		}

		private void FillPopup(Task<SuggestionFieldPendingLoad> itemsSourceTask) {
			SuggestionFieldPendingLoad result = itemsSourceTask.Result;
			FillResultFunc(result.Result, result.IsVisibilityAffected);
			if (mPendingLoad != null) {
				LoadPendingPopupData();
			}
		}
	}
}
