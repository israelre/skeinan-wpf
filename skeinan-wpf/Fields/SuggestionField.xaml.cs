﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace SkeinanWpf.Fields {
	public partial class SuggestionField : Field {
		private List<BindingManager> BindingManagers { get; } = new List<BindingManager>();

		public Func<string, IList> LoadFunc { get; set; }

		public static readonly DependencyProperty SelectedValuesProperty =
				DependencyProperty.Register(nameof(SelectedValues), typeof(IBindingList), typeof(SuggestionField), new PropertyMetadata(SelectedValuesChanged));
		public IBindingList SelectedValues {
			get { return (IBindingList)GetValue(SelectedValuesProperty); }
			set { SetValue(SelectedValuesProperty, value); }
		}

		public static readonly DependencyProperty DisplayMemberPathProperty =
				DependencyProperty.Register(nameof(DisplayMemberPath), typeof(string), typeof(SuggestionField));
		public string DisplayMemberPath {
			get { return (string)GetValue(DisplayMemberPathProperty); }
			set { SetValue(DisplayMemberPathProperty, value); }
		}

		public static readonly DependencyProperty MinItemCountProperty =
				DependencyProperty.Register(nameof(MinItemCount), typeof(int), typeof(SuggestionField), new PropertyMetadata(MinItemCountChanged));
		public int MinItemCount {
			get { return (int)GetValue(MinItemCountProperty); }
			set { SetValue(MinItemCountProperty, value); }
		}

		public static readonly DependencyProperty MaxItemCountProperty =
				DependencyProperty.Register(nameof(MaxItemCount), typeof(int), typeof(SuggestionField), new PropertyMetadata(MaxItemCountPropertyChanged));
		public int MaxItemCount {
			get { return (int)GetValue(MaxItemCountProperty); }
			set { SetValue(MaxItemCountProperty, value); }
		}

		public static readonly DependencyProperty InnerSpacingProperty =
				DependencyProperty.Register(nameof(InnerSpacing), typeof(double), typeof(SuggestionField));
		public double InnerSpacing {
			get { return (double)GetValue(InnerSpacingProperty); }
			set { SetValue(InnerSpacingProperty, value); }
		}

		public static readonly RoutedEvent FullValidateEvent =
				EventManager.RegisterRoutedEvent(nameof(FullValidate), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(SuggestionFieldElement));
		public event RoutedEventHandler FullValidate {
			add { AddHandler(FullValidateEvent, value); }
			remove { RemoveHandler(FullValidateEvent, value); }
		}

		private static void MaxItemCountPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			SuggestionField field = (SuggestionField)d;
			field.RestrictItemsCount();
		}

		private static void MinItemCountChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			SuggestionField field = (SuggestionField)d;
			field.RestrictItemsCount();
		}

		private static void SelectedValuesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			SuggestionField field = (SuggestionField)d;
			if (field.SelectedValues != null) {
				field.SelectedValues.ListChanged += field.SelectedValues_ListChanged;
			}
			field.SelectedValuesChanged();
		}

		public SuggestionField() {
			InitializeComponent();
			Loaded += SuggestionField_Loaded;
			AddButton.Click += AddButton_Click;
		}

		private void SelectedValuesChanged() {
			RestrictItemsCount();
			foreach (SuggestionFieldElement element in BoxesStackPanel.Children) {
				element.RaiseValidation();
			}
			RaiseEvent(new RoutedEventArgs(FullValidateEvent, this));
		}

		private void RestrictItemsCount() {
			while (MaxItemCount != 0 && SelectedValues?.Count > MaxItemCount) {
				SelectedValues.RemoveAt(SelectedValues.Count - 1);
			}
			while (SelectedValues?.Count < MinItemCount) {
				SelectedValues.Add(null);
			}
			RefreshControlCount();
		}

		private void RefreshControlCount() {
			int itemsCount = SelectedValues != null ? SelectedValues.Count : 0;
			int controlsCount = BoxesStackPanel.Children.Count;
			int valuesDifference = itemsCount - controlsCount;
			for (int i = 0; i < valuesDifference; ++i) {
				AddComboBox();
			}
			if (valuesDifference < 0) {
				BoxesStackPanel.Children.RemoveRange(itemsCount, -valuesDifference);
			}
			if (valuesDifference != 0) {
				BindingManagers.ForEach(bm => bm.ClearBinding());
				for (int i = 0; i < BoxesStackPanel.Children.Count; ++i) {
					SuggestionFieldElement element = (SuggestionFieldElement)BoxesStackPanel.Children[i];
					BindingManagers.Add(new BindingManager(element, SuggestionFieldElement.SelectedValueProperty, this, nameof(SelectedValues) + "[" + i + "]") {
						UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
					}.Bind());
					element.IsDeleteVisible = BoxesStackPanel.Children.Count != MinItemCount;
				}
				UpdateMargins();
			}
		}

		private SuggestionFieldElement AddComboBox() {
			SuggestionFieldElement element = new SuggestionFieldElement {
				InnerSpacing = InnerSpacing
			};

			int index = BoxesStackPanel.Children.Count;
			new BindingManager(element, SuggestionFieldElement.DisplayMemberPathProperty, this, nameof(DisplayMemberPath)).Bind();
			SelectedValues.ListChanged += SelectedValues_ListChanged;
			element.ItemDeleted += ItemDeleted;
			element.LoadFunc = LoadFunc;

			BoxesStackPanel.Children.Add(element);
			return element;
		}

		private void ItemDeleted(object sender, RoutedEventArgs e) {
			int index = BoxesStackPanel.Children.IndexOf((UIElement)sender);
			if (SelectedValues.Count > MinItemCount) {
				SelectedValues.RemoveAt(index);
			}
			if (SelectedValues.Count > 0) {
				if (index < SelectedValues.Count) {
					Keyboard.Focus(BoxesStackPanel.Children[index]);
				} else {
					Keyboard.Focus(BoxesStackPanel.Children[index - 1]);
				}
			} else {
				Keyboard.Focus(AddButton);
			}
		}

		private void AddButton_Click(object sender, RoutedEventArgs e) {
			SelectedValues?.Add(null);
			SuggestionFieldElement newFocus = BoxesStackPanel.Children.OfType<SuggestionFieldElement>().LastOrDefault();
			if (newFocus != null) Keyboard.Focus(newFocus);
		}

		private void SuggestionField_Loaded(object sender, RoutedEventArgs e) {
			RestrictItemsCount();
		}

		private void UpdateMargins() {
			if (BoxesStackPanel.Children.Count == 0) {
				AddButton.Margin = new Thickness(0);
			} else {
				Thickness margin = new Thickness(0, InnerSpacing, 0, 0);
				bool isFirst = true;
				foreach (FrameworkElement element in BoxesStackPanel.Children) {
					if (isFirst) {
						element.Margin = new Thickness(0);
						isFirst = false;
					} else {
						element.Margin = margin;
					}
				}
				AddButton.Margin = margin;
			}
		}

		private void SelectedValues_ListChanged(object sender, ListChangedEventArgs e) {
			BindingOperations.GetBindingExpression(this, SelectedValuesProperty).UpdateSource();
			SelectedValuesChanged();
		}
	}
}
