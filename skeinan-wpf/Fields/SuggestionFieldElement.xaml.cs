﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;

namespace SkeinanWpf.Fields {
	public partial class SuggestionFieldElement : Field {
		private bool mIsConfirming;
		private bool mIsUpdatingText;

		private SuggestionFieldTask mSuggestionTask;
		protected SuggestionFieldTask SuggestionTask {
			get {
				if (mSuggestionTask == null) {
					mSuggestionTask = new SuggestionFieldTask(LoadFunc, FillResult);
				}
				return mSuggestionTask;
			}
		}

		public Func<string, IList> LoadFunc { get; set; }

		public static readonly DependencyProperty DisplayMemberPathProperty =
				DependencyProperty.Register(nameof(DisplayMemberPath), typeof(string), typeof(SuggestionFieldElement));
		public string DisplayMemberPath {
			get { return (string)GetValue(DisplayMemberPathProperty); }
			set { SetValue(DisplayMemberPathProperty, value); }
		}

		public static readonly DependencyProperty SelectedValueProperty =
				DependencyProperty.Register(nameof(SelectedValue), typeof(object), typeof(SuggestionFieldElement), new PropertyMetadata(SelectedValueChangedCallback));
		public object SelectedValue {
			get { return GetValue(SelectedValueProperty); }
			set { SetValue(SelectedValueProperty, value); }
		}

		private static void SelectedValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			((SuggestionFieldElement)d).SelectedValueChangedCallback();
		}

		private void SelectedValueChangedCallback() {
			UpdateText();
		}

		public static readonly DependencyProperty IsDeleteVisibleProperty =
				DependencyProperty.Register(nameof(IsDeleteVisible), typeof(bool), typeof(SuggestionFieldElement), new PropertyMetadata(true));
		public object IsDeleteVisible {
			get { return GetValue(IsDeleteVisibleProperty); }
			set { SetValue(IsDeleteVisibleProperty, value); }
		}

		private static readonly DependencyProperty IsSuggestionBoxVisibleProperty =
				DependencyProperty.Register(nameof(IsSuggestionBoxVisible), typeof(bool), typeof(SuggestionFieldElement), new PropertyMetadata(false));
		private bool IsSuggestionBoxVisible {
			get { return (bool)GetValue(IsSuggestionBoxVisibleProperty); }
			set { SetValue(IsSuggestionBoxVisibleProperty, value); }
		}

		public static readonly DependencyProperty InnerSpacingProperty =
				DependencyProperty.Register(nameof(InnerSpacing), typeof(double), typeof(SuggestionFieldElement));
		public double InnerSpacing {
			get { return (double)GetValue(InnerSpacingProperty); }
			set { SetValue(InnerSpacingProperty, value); }
		}

		public static readonly RoutedEvent ItemDeletedEvent =
				EventManager.RegisterRoutedEvent(nameof(ItemDeleted), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(SuggestionFieldElement));
		public event RoutedEventHandler ItemDeleted {
			add { AddHandler(ItemDeletedEvent, value); }
			remove { RemoveHandler(ItemDeletedEvent, value); }
		}

		public static readonly RoutedEvent TextInsertedEvent =
				EventManager.RegisterRoutedEvent(nameof(TextInserted), RoutingStrategy.Bubble, typeof(TextInsertedHandler), typeof(SuggestionFieldElement));
		public event TextInsertedHandler TextInserted {
			add { AddHandler(TextInsertedEvent, value); }
			remove { RemoveHandler(TextInsertedEvent, value); }
		}

		public static readonly RoutedEvent ValidateEvent =
				EventManager.RegisterRoutedEvent(nameof(Validate), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(SuggestionFieldElement));
		public event RoutedEventHandler Validate {
			add { AddHandler(ValidateEvent, value); }
			remove { RemoveHandler(ValidateEvent, value); }
		}

		public SuggestionFieldElement() {
			InitializeComponent();
			new BindingManager(SuggestionListBox, ListBox.DisplayMemberPathProperty, this, nameof(DisplayMemberPath)).Bind();
			new BindingManager(SuggestionPopup, Popup.IsOpenProperty, this, nameof(IsSuggestionBoxVisible)).Bind();
			new BindingManager(DeleteButton, Button.VisibilityProperty, this, nameof(IsDeleteVisible)) {
				Converter = new BooleanToVisibilityConverter()
			}.Bind();

			Loaded += SuggestionFieldElement_Loaded;
			MainTextBox.PreviewKeyDown += SuggestionFieldElement_PreviewKeyDown;

			MainTextBox.IsKeyboardFocusWithinChanged += MainTextBox_IsKeyboardFocusWithinChanged;
			MainTextBox.PreviewMouseDown += MainTextBox_PreviewMouseDown;
			MainTextBox.TextChanged += MainTextBox_TextChanged;

			SuggestionListBox.PreviewMouseUp += SuggestionListBox_PreviewMouseUp;

			DeleteButton.Click += DeleteButton_Click;
		}

		private void UpdateText() {
			string selectedValueText = SelectedValue?.GetType().GetProperty(DisplayMemberPath).GetValue(SelectedValue).ToString();
			if (MainTextBox.Text != selectedValueText) {
				mIsUpdatingText = true;
				MainTextBox.Text = selectedValueText;
				mIsUpdatingText= false;
			}
		}

		private void SuggestionFieldElement_Loaded(object sender, RoutedEventArgs e) {
			DeleteButton.Margin = new Thickness(InnerSpacing, 0, 0, 0);
			Window.GetWindow(this).LocationChanged -= SuggestionFieldElement_LocationChanged;
			Window.GetWindow(this).LocationChanged += SuggestionFieldElement_LocationChanged;
			ScrollViewer scrollViewer = WpfUtil.GetNearestParent<ScrollViewer>(this);
			if (scrollViewer != null) {
				scrollViewer.ScrollChanged -= ScrollViewer_ScrollChanged;
				scrollViewer.ScrollChanged += ScrollViewer_ScrollChanged;
			}
		}

		private void FillResult(IList result, bool visibilityAffected) {
			Dispatcher.Invoke(() => {
				SuggestionListBox.ItemsSource = result;
				if (visibilityAffected) {
					RefreshSuggestionListVisibility();
				}
			});
		}

		private void SuggestionFieldElement_PreviewKeyDown(object sender, KeyEventArgs e) {
			switch (e.Key) {
				case Key.Down:
					ChangeSuggestionIndex(SuggestionListBox.SelectedIndex + 1);
					break;
				case Key.Up:
					ChangeSuggestionIndex(SuggestionListBox.SelectedIndex - 1);
					break;
				case Key.Escape:
					IsSuggestionBoxVisible = false;
					break;
				case Key.Enter:
					if (IsSuggestionBoxVisible && SuggestionListBox.SelectedValue != null) {
						ConfirmSelection();
						e.Handled = true;
					}
					break;
				case Key.Tab:
					if (IsSuggestionBoxVisible && SuggestionListBox.SelectedValue != null) {
						ConfirmSelection();
					}
					break;
			}
		}

		private void MainTextBox_TextChanged(object sender, TextChangedEventArgs e) {
			SuggestionTask.LoadPopupData(MainTextBox.Text, AllowVisibilityChange());
		}

		public void RaiseValidation() {
			RaiseEvent(new RoutedEventArgs(ValidateEvent, this));
		}

		private void MainTextBox_IsKeyboardFocusWithinChanged(object sender, DependencyPropertyChangedEventArgs e) {
			OnFocusChanged(MainTextBox.IsKeyboardFocusWithin);
		}

		private void OnFocusChanged(bool hasFocus) {
			if (AllowVisibilityChange()) {
				if (hasFocus) {
					SuggestionTask.LoadPopupData(MainTextBox.Text, true);
				} else {
					if (!HasTextBoxFocus()) {
						ValidateSelectedValue();
						RefreshSuggestionListVisibility();
					}
				}
			}
		}

		private void SuggestionFieldElement_LocationChanged(object sender, EventArgs e) {
			RefreshSuggestionListLocation();
		}

		private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e) {
			if (e.VerticalChange != 0) {
				RefreshSuggestionListLocation();
				RefreshSuggestionListVisibility(true);
			}
		}

		private void MainTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e) {
			if (IsSuggestionBoxVisible) {
				IsSuggestionBoxVisible = false;
			} else {
				RefreshSuggestionListVisibility();
			}
		}

		private void SuggestionListBox_PreviewMouseUp(object sender, MouseButtonEventArgs e) {
			ConfirmSelection();
		}

		private void DeleteButton_Click(object sender, RoutedEventArgs e) {
			RaiseEvent(new RoutedEventArgs(ItemDeletedEvent, this));
		}

		private void ConfirmSelection() {
			mIsConfirming = true;
			SelectedValue = SuggestionListBox.SelectedValue;
			UpdateText();
			Keyboard.Focus(MainTextBox);
			MainTextBox.SelectAll();
			IsSuggestionBoxVisible = false;
			mIsConfirming = false;
		}

		private void ChangeSuggestionIndex(int newIndex) {
			int? itemCount = (SuggestionListBox.ItemsSource as IList)?.Count;
			if (newIndex >= itemCount) {
				newIndex = 0;
			} else if (newIndex < 0) {
				newIndex = (itemCount ?? 0) - 1;
			}
			SuggestionListBox.SelectedIndex = newIndex;
			RefreshSuggestionListVisibility();
		}

		private void RefreshSuggestionListLocation() {
			SuggestionPopup.HorizontalOffset += 1;
			SuggestionPopup.HorizontalOffset -= 1;
		}

		private void RefreshSuggestionListVisibility(bool hideOnly = false) {
			bool visible = HasTextBoxFocus();
			visible = visible && AllowVisibilityChange();
			visible = visible && IsInWindow(this, Window.GetWindow(this));
			visible = visible && (SuggestionListBox.ItemsSource as IList)?.Count > 0;
			IsSuggestionBoxVisible = (IsSuggestionBoxVisible || !hideOnly) && visible;
		}

		private bool IsInWindow(FrameworkElement component, Window window) {
			Point componentTopLeft = component.PointFromScreen(new Point(0, 0));
			componentTopLeft.X *= -1;
			componentTopLeft.Y *= -1;
			Point componentBottomRight = new Point(componentTopLeft.X + component.ActualWidth, componentTopLeft.Y + component.ActualHeight);

			Point windowTopLeft = window.PointFromScreen(new Point(0, 0));
			windowTopLeft.X *= -1;
			windowTopLeft.Y *= -1;
			Point windowBottomRight = new Point(windowTopLeft.X + window.ActualWidth, windowTopLeft.Y + window.ActualHeight);

			Rect windowRect = new Rect(windowTopLeft, windowBottomRight);
			Rect componentRect = new Rect(componentTopLeft, componentBottomRight);

			return windowRect.IntersectsWith(componentRect);
		}

		private void ValidateSelectedValue() {
			MainTextBox.Text = MainTextBox.Text?.Trim();
			if (Window.GetWindow(this).IsVisible) {
				if (String.IsNullOrEmpty(MainTextBox.Text)) {
					SelectedValue = null;
				} else {
					TextInsertedEventArgs e = new TextInsertedEventArgs(TextInsertedEvent, MainTextBox.Text);
					RaiseEvent(e);
					SelectedValue = e.SelectedValue;
					if (SelectedValue == null) {
						MainTextBox.Text = "";
					}
				}
			}
		}

		private bool HasKeyboardFocus() {
			return IsKeyboardFocusWithin || WpfUtil.IsVisualChild(SuggestionPopup, Keyboard.FocusedElement as FrameworkElement);
		}

		private bool HasTextBoxFocus() {
			return MainTextBox.IsKeyboardFocusWithin || WpfUtil.IsVisualChild(SuggestionPopup, Keyboard.FocusedElement as FrameworkElement);
		}

		private bool AllowVisibilityChange() {
			return !mIsUpdatingText && !mIsConfirming;
		}
	}
}
