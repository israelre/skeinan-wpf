﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SkeinanWpf.Fields {
	public abstract class Field : UserControl, INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;
		public PropertyChangeManager PropertyChangeManager { get; }

		public void NotifyPropertyChanged([CallerMemberName] String propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void AddListener(string propertyName, Action action) {
			PropertyChangeManager.AddListener(propertyName, action);
		}

		private void Field_Loaded(object sender, RoutedEventArgs e) {
			AdjustFocus();
		}

		private void Field_GotFocus(object sender, RoutedEventArgs e) {
			AdjustFocus();
		}

		protected void AdjustFocus() {
			if (IsLoaded && Keyboard.FocusedElement == this) {
				UIElement firstChild = WpfUtil.FindChildren((DependencyObject)Content).OfType<UIElement>().Where(c => c.Focusable).FirstOrDefault();
				Keyboard.Focus(firstChild);
			}
		}

		public Field() {
			PropertyChangeManager = new PropertyChangeManager(this);
			Focusable = true;
			Initialized += Field_Initialized;
			GotFocus += Field_GotFocus;
			Loaded += Field_Loaded;
		}

		private void Field_Initialized(object sender, EventArgs e) {
			((FrameworkElement)Content).DataContext = this;
		}
	}
}
