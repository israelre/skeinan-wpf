﻿using System.Collections;

namespace SkeinanWpf.Fields {
	public class SuggestionFieldPendingLoad {
		public string Input { get; set; }
		public bool IsVisibilityAffected { get; set; }
		public IList Result { get; set; }

		public SuggestionFieldPendingLoad(string input, bool isVisibilityAffected) {
			Input = input;
			IsVisibilityAffected = isVisibilityAffected;
		}
	}
}
