﻿using System;
using System.Windows.Input;

namespace SkeinanWpf {
	public class ActionCommand<T> : ICommand {
		public event EventHandler CanExecuteChanged;
		private Action<T> mAction;

		public ActionCommand(Action<T> action) {
			mAction = action;
		}

		public bool CanExecute(object parameter) {
			return true;
		}

		public void Execute(object parameter) {
			mAction((T)parameter);
		}
	}
}
