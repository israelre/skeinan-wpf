﻿using System;
using System.Windows;

namespace SkeinanWpf {
	public class TextInsertedEventArgs : RoutedEventArgs {
		public string Text { get; set; }
		public object SelectedValue { get; set; }

		public TextInsertedEventArgs(string text) : base() {
			Text = text;
		}

		public TextInsertedEventArgs(RoutedEvent routedEvent, string text) : base(routedEvent) {
			Text = text;
		}

		public TextInsertedEventArgs(RoutedEvent routedEvent, object source, string text) : base(routedEvent, source) {
			Text = text;
		}
	}
}
